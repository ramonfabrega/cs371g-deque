// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);}
    return b;}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;}}
    catch (...) {
        my_destroy(a, p, x);
        throw;}
    return x;}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;}}
    catch (...) {
        my_destroy(a, p, b);
        throw;}}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * your documentation
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        // <your code>
        // you must use std::equal()
        return true;}

    // ----------
    // operator <
    // ----------

    /**
     * your documentation
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        // <your code>
        // you must use std::lexicographical_compare()
        return true;}

    // ----
    // swap
    // ----

    /**
     * your documentation
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);}

    public:
        // ------
        // usings
        // ------

        // you must use this allocator for the inner arrays
        using allocator_type  = A;
        using value_type      = typename allocator_type::value_type;

        using size_type       = typename allocator_type::size_type;
        using difference_type = typename allocator_type::difference_type;

        using pointer         = typename allocator_type::pointer;
        using const_pointer   = typename allocator_type::const_pointer;

        using reference       = typename allocator_type::reference;
        using const_reference = typename allocator_type::const_reference;

        // you must use this allocator for the outer array
        using allocator_type_2 = typename A::template rebind<pointer>::other;

    private:
        // ----
        // data
        // ----

        allocator_type _a;

        // <your data>

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
            return true;}

    public:
        // --------
        // iterator
        // --------

        class iterator {
            // -----------
            // operator ==
            // -----------

            /**
             * your documentation
             */
            friend bool operator == (const iterator&, const iterator&) {
                // <your code>
                return true;} // fix

            /**
             * your documentation
             */
            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}

            // ----------
            // operator +
            // ----------

            /**
             * your documentation
             */
            friend iterator operator + (iterator lhs, difference_type rhs) {
                return lhs += rhs;}

            // ----------
            // operator -
            // ----------

            /**
             * your documentation
             */
            friend iterator operator - (iterator lhs, difference_type rhs) {
                return lhs -= rhs;}

            public:
                // ------
                // usings
                // ------

                // this requires a weaker iterator than the real deque provides
                using iterator_category = std::bidirectional_iterator_tag;
                using value_type        = typename my_deque::value_type;
                using difference_type   = typename my_deque::difference_type;
                using pointer           = typename my_deque::pointer;
                using reference         = typename my_deque::reference;

            private:
                // ----
                // data
                // ----

                // <your data>

            private:
                // -----
                // valid
                // -----

                bool valid () const {
                    // <your code>
                    return true;}

            public:
                // -----------
                // constructor
                // -----------

                /**
                 * your documentation
                 */
                iterator (/* <your arguments> */) {
                    // <your code>
                    assert(valid());}

                iterator             (const iterator&) = default;
                ~iterator            ()                = default;
                iterator& operator = (const iterator&) = default;

                // ----------
                // operator *
                // ----------

                /**
                 * your documentation
                 */
                reference operator * () const {
                    // <your code>
                    static value_type dummy; // remove
                    return dummy;}           // fix

                // -----------
                // operator ->
                // -----------

                /**
                 * your documentation
                 */
                pointer operator -> () const {
                    return &**this;}

                // -----------
                // operator ++
                // -----------

                /**
                 * your documentation
                 */
                iterator& operator ++ () {
                    // <your code>
                    assert(valid());
                    return *this;}

                /**
                 * your documentation
                 */
                iterator operator ++ (int) {
                    iterator x = *this;
                    ++(*this);
                    assert(valid());
                    return x;}

                // -----------
                // operator --
                // -----------

                /**
                 * your documentation
                 */
                iterator& operator -- () {
                    // <your code>
                    assert(valid());
                    return *this;}

                /**
                 * your documentation
                 */
                iterator operator -- (int) {
                    iterator x = *this;
                    --(*this);
                    assert(valid());
                    return x;}

                // -----------
                // operator +=
                // -----------

                /**
                 * your documentation
                 */
                iterator& operator += (difference_type d) {
                    // <your code>
                    assert(valid());
                    return *this;}

                // -----------
                // operator -=
                // -----------

                /**
                 * your documentation
                 */
                iterator& operator -= (difference_type d) {
                    // <your code>
                    assert(valid());
                    return *this;}};

    public:
        // --------------
        // const_iterator
        // --------------

        class const_iterator {
            // -----------
            // operator ==
            // -----------

            /**
             * your documentation
             */
            friend bool operator == (const const_iterator&, const const_iterator&) {
                // <your code>
                return true;} // fix

            /**
             * your documentation
             */
            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);}

            // ----------
            // operator +
            // ----------

            /**
             * your documentation
             */
            friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
                return lhs += rhs;}

            // ----------
            // operator -
            // ----------

            /**
             * your documentation
             */
            friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
                return lhs -= rhs;}

            public:
                // ------
                // usings
                // ------

                // this requires a weaker iterator than the real deque provides
                using iterator_category = std::bidirectional_iterator_tag;
                using value_type        = typename my_deque::value_type;
                using difference_type   = typename my_deque::difference_type;
                using pointer           = typename my_deque::const_pointer;
                using reference         = typename my_deque::const_reference;

            private:
                // ----
                // data
                // ----

                // <your data>

            private:
                // -----
                // valid
                // -----

                bool valid () const {
                    // <your code>
                    return true;}

            public:
                // -----------
                // constructor
                // -----------

                /**
                 * your documentation
                 */
                const_iterator (/* <your arguments> */) {
                    // <your code>
                    assert(valid());}

                const_iterator             (const const_iterator&) = default;
                ~const_iterator            ()                      = default;
                const_iterator& operator = (const const_iterator&) = default;

                // ----------
                // operator *
                // ----------

                /**
                 * your documentation
                 */
                reference operator * () const {
                    // <your code>
                    static value_type dummy; // remove
                    return dummy;}           // fix

                // -----------
                // operator ->
                // -----------

                /**
                 * your documentation
                 */
                pointer operator -> () const {
                    return &**this;}

                // -----------
                // operator ++
                // -----------

                /**
                 * your documentation
                 */
                const_iterator& operator ++ () {
                    // <your code>
                    assert(valid());
                    return *this;}

                /**
                 * your documentation
                 */
                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++(*this);
                    assert(valid());
                    return x;}

                // -----------
                // operator --
                // -----------

                /**
                 * your documentation
                 */
                const_iterator& operator -- () {
                    // <your code>
                    assert(valid());
                    return *this;}

                /**
                 * your documentation
                 */
                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --(*this);
                    assert(valid());
                    return x;}

                // -----------
                // operator +=
                // -----------

                /**
                 * your documentation
                 */
                const_iterator& operator += (difference_type) {
                    // <your code>
                    assert(valid());
                    return *this;}

                // -----------
                // operator -=
                // -----------

                /**
                 * your documentation
                 */
                const_iterator& operator -= (difference_type) {
                    // <your code>
                    assert(valid());
                    return *this;}};

    public:
        // ------------
        // constructors
        // ------------

        my_deque () = default;

        /**
         * your documentation
         */
        explicit my_deque (size_type s) {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        my_deque (size_type s, const_reference v) {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        my_deque (size_type s, const_reference v, const allocator_type& a) {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        my_deque (std::initializer_list<value_type> rhs) {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        my_deque (const my_deque& that) {
            // <your code>
            assert(valid());}

        // ----------
        // destructor
        // ----------

        /**
         * your documentation
         */
        ~my_deque () {
            // <your code>
            assert(valid());}

        // ----------
        // operator =
        // ----------

        /**
         * your documentation
         */
        my_deque& operator = (const my_deque& rhs) {
            // <your code>
            assert(valid());
            return *this;}

        // -----------
        // operator []
        // -----------

        /**
         * your documentation
         */
        reference operator [] (size_type index) {
            // <your code>
            static value_type dummy; // remove
            return dummy;}           // fix

        /**
         * your documentation
         */
        const_reference operator [] (size_type index) const {
            return const_cast<my_deque*>(this)->operator[](index);}

        // --
        // at
        // --

        /**
         * your documentation
         * @throws out_of_range
         */
        reference at (size_type index) {
            // <your code>
            static value_type dummy; // remove
            return dummy;}           // fix

        /**
         * your documentation
         * @throws out_of_range
         */
        const_reference at (size_type index) const {
            return const_cast<my_deque*>(this)->at(index);}

        // ----
        // back
        // ----

        /**
         * your documentation
         */
        reference back () {
            // <your code>
            static value_type dummy; // remove
            return dummy;}           // fix

        /**
         * your documentation
         */
        const_reference back () const {
            return const_cast<my_deque*>(this)->back();}

        // -----
        // begin
        // -----

        /**
         * your documentation
         */
        iterator begin () {
            // <your code>
            return iterator(/* <your arguments> */);}

        /**
         * your documentation
         */
        const_iterator begin () const {
            // <your code>
            return const_iterator(/* <your arguments> */);}

        // -----
        // clear
        // -----

        /**
         * your documentation
         */
        void clear () {
            // <your code>
            assert(valid());}

        // -----
        // empty
        // -----

        /**
         * your documentation
         */
        bool empty () const {
            return !size();}

        // ---
        // end
        // ---

        /**
         * your documentation
         */
        iterator end () {
            // <your code>
            return iterator(/* <your arguments> */);}

        /**
         * your documentation
         */
        const_iterator end () const {
            // <your code>
            return const_iterator(/* <your arguments> */);}

        // -----
        // erase
        // -----

        /**
         * your documentation
         */
        iterator erase (iterator) {
            // <your code>
            assert(valid());
            return iterator();}

        // -----
        // front
        // -----

        /**
         * your documentation
         */
        reference front () {
            // <your code>
            static value_type dummy; // remove
            return dummy;}           // fix

        /**
         * your documentation
         */
        const_reference front () const {
            return const_cast<my_deque*>(this)->front();}

        // ------
        // insert
        // ------

        /**
         * your documentation
         */
        iterator insert (iterator, const_reference) {
            // <your code>
            assert(valid());
            return iterator();}

        // ---
        // pop
        // ---

        /**
         * your documentation
         */
        void pop_back () {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        void pop_front () {
            // <your code>
            assert(valid());}

        // ----
        // push
        // ----

        /**
         * your documentation
         */
        void push_back (const_reference) {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        void push_front (const_reference) {
            // <your code>
            assert(valid());}

        // ------
        // resize
        // ------

        /**
         * your documentation
         */
        void resize (size_type s) {
            // <your code>
            assert(valid());}

        /**
         * your documentation
         */
        void resize (size_type, const_reference v) {
            // <your code>
            assert(valid());}

        // ----
        // size
        // ----

        /**
         * your documentation
         */
        size_type size () const {
            // <your code>
            return 0;}

        // ----
        // swap
        // ----

        /**
         * your documentation
         */
        void swap (my_deque&) {
            // <your code>
            assert(valid());}};

#endif // Deque_h
